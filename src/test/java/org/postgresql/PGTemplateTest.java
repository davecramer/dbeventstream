package org.postgresql;


import org.postgresql.replication.LogSequenceNumber;
import org.postgresql.replication.PGReplicationStream;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;
import java.util.Properties;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=DataConfig.class)
public class PGTemplateTest {

  @Autowired
  PGTemplate pgTemplate;

  @Before
  public void setup() throws SQLException {
    pgTemplate.createLogicalReplicationSlot("test", "test_decoding");
  }

  @After
  public void tearDown() throws  Exception{
    pgTemplate.dropReplicationSlot("test");
  }

  @Test
  public void testIsSlotInActive() throws Exception {
    Assert.assertFalse(pgTemplate.isReplicationSlotActive("foo"));
  }
  @Test
  public void testSlotExists() throws Exception {
    Assert.assertFalse(pgTemplate.slotExists("foo"));
    Assert.assertTrue(pgTemplate.slotExists("test"));
  }
  @Test
  public void testStopSlot() throws  Exception {
    Assert.assertFalse(pgTemplate.stopSlot("foo"));
    Assert.assertFalse(pgTemplate.stopSlot("test"));
  }
  @Test
  public void testDropReplicationSlot() throws  Exception {
    Assert.assertFalse(pgTemplate.dropReplicationSlot("foo"));
  }
  @Test
  public void testGetReplicationStream() throws  Exception{

    PGReplicationStream replicationStream = pgTemplate.getReplicationStream("test",
        LogSequenceNumber.INVALID_LSN,
        new Properties());
    Assert.assertNotNull(replicationStream);


  }
}
