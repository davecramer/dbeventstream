package org.postgresql.listener;

@FunctionalInterface
public interface PostgresLogicalDecoderErrorHandler {
  void handleError(Throwable th);
}
