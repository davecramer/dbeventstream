package org.postgresql.listener;

import org.postgresql.LogicalDecoder;
import org.postgresql.PGTemplate;
import org.postgresql.annotation.PostgresLogicalDecoderListener;
import org.postgresql.configuration.MethodPostgresLogicalDecodingListenerSlot;
import org.postgresql.encoder.Encoder;
import org.postgresql.replication.LogSequenceNumber;
import org.postgresql.replication.PGReplicationStream;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.SmartLifecycle;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class PostgresLogicalDecoderListenerContainer implements SmartLifecycle {

  PGTemplate pgTemplate;

  private boolean stop = false;
  private boolean running = false;
  private Encoder encoder;
  private  String slotName;
  private LogSequenceNumber lsn;
  private PGReplicationStream replicationStream;
  Method methodToCall;
  MethodPostgresLogicalDecodingListenerSlot slot;
  Object bean;

  public PostgresLogicalDecoderListenerContainer( MethodPostgresLogicalDecodingListenerSlot slot, Object bean, PostgresLogicalDecoderListener listener) throws
      SQLException {
    methodToCall = slot.getMethod();
    slotName = listener.slotName();
    this.slot =  slot;
    this.bean = bean;


    encoder = (Encoder)slot.getBeanFactory().getBean(listener.decoder());
    if (encoder == null ) {
      throw new SQLException("Error encoder " + listener.decoder() + " not found");
    }

    if ( pgTemplate == null ) {
      pgTemplate = (PGTemplate)slot.getBeanFactory().getBean("pgTemplate");
    }
    try {
      pgTemplate.dropReplicationSlot(slotName);
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (TimeoutException e) {
      e.printStackTrace();
    }
    lsn = pgTemplate.createLogicalReplicationSlot(slotName, encoder.getName());
  }
  public PostgresLogicalDecoderListenerContainer addListener( PostgresLogicalDecoderListener listener){
    return this;
  }

  void doStart(){
    if ( isRunning() ){
      return;
    }
    try {
      replicationStream = pgTemplate.getReplicationStream(slotName, lsn, encoder.getSlotOptions());
    } catch (SQLException ex ){
      ex.printStackTrace();
    }
    SimpleAsyncTaskExecutor consumerExecutor = new SimpleAsyncTaskExecutor(
        (slotName) + "-C-");
    consumerExecutor.execute(new Runnable() {
      @Override
      public void run() {
        while (!stop) {
          try {
            String json = pgTemplate.readStream(replicationStream, new LogicalDecoder<String>() {

              @Override
              public String decode(byte[] buf, int offset, int length) {
                String s = new String(buf, offset, length);
                return s;
              }
            });
            if (json != null) {
              Object obj = methodToCall.invoke(bean, json) ;
              if (((Boolean)obj).booleanValue()==true){

                replicationStream.setAppliedLSN(replicationStream.getLastReceiveLSN());
                replicationStream.setFlushedLSN(replicationStream.getLastReceiveLSN());
              }
            }
            TimeUnit.MILLISECONDS.sleep(10);

          } catch (Throwable e) {
            PostgresLogicalDecoderErrorHandler errorHandler = slot.getErrorHandler();
            if (errorHandler != null) {
              errorHandler.handleError(e);
            }
          }

        }
      }
    });
  }
  /**
   * Start this component.
   * <p>Should not throw an exception if the component is already running.
   * <p>In the case of a container, this will propagate the start signal to all
   * components that apply.
   *
   * @see SmartLifecycle#isAutoStartup()
   */
  @Override
  public void start() {
    doStart();
  }

  /**
   * Stop this component, typically in a synchronous fashion, such that the component is fully
   * stopped upon return of this method. Consider implementing {@link SmartLifecycle} and its {@code
   * stop(Runnable)} variant when asynchronous stop behavior is necessary.
   * <p>Note that this stop notification is not guaranteed to come before destruction: On
   * regular shutdown, {@code Lifecycle} beans will first receive a stop notification before the
   * general destruction callbacks are being propagated; however, on hot refresh during a context's
   * lifetime or on aborted refresh attempts, only destroy methods will be called.
   * <p>Should not throw an exception if the component isn't started yet.
   * <p>In the case of a container, this will propagate the stop signal to all components
   * that apply.
   *
   * @see SmartLifecycle#stop(Runnable)
   * @see DisposableBean#destroy()
   */
  @Override
  public void stop() {
    stop=true;
  }

  /**
   * Check whether this component is currently running.
   * <p>In the case of a container, this will return {@code true} only if <i>all</i>
   * components that apply are currently running.
   *
   * @return whether the component is currently running
   */
  @Override
  public boolean isRunning() {
    return running;
  }

  /**
   * Returns {@code true} if this {@code Lifecycle} component should get started automatically by
   * the container at the time that the containing {@link ApplicationContext} gets refreshed.
   * <p>A value of {@code false} indicates that the component is intended to
   * be started through an explicit {@link #start()} call instead, analogous to a plain {@link
   * Lifecycle} implementation.
   *
   * @see #start()
   * @see #getPhase()
   * @see LifecycleProcessor#onRefresh()
   * @see ConfigurableApplicationContext#refresh()
   */
  @Override
  public boolean isAutoStartup() {
    return true;
  }

  /**
   * Indicates that a Lifecycle component must stop if it is currently running.
   * <p>The provided callback is used by the {@link LifecycleProcessor} to support
   * an ordered, and potentially concurrent, shutdown of all components having a common shutdown
   * order value. The callback <b>must</b> be executed after the {@code SmartLifecycle} component
   * does indeed stop.
   * <p>The {@link LifecycleProcessor} will call <i>only</i> this variant of the
   * {@code stop} method; i.e. {@link Lifecycle#stop()} will not be called for {@code
   * SmartLifecycle} implementations unless explicitly delegated to within the implementation of
   * this method.
   *
   * @see #stop()
   * @see #getPhase()
   */
  @Override
  public void stop(Runnable callback) {

  }

  /**
   * Return the phase value of this object.
   */
  @Override
  public int getPhase() {
    return 0;
  }
}
