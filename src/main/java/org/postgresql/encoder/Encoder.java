package org.postgresql.encoder;

import java.util.Properties;

public interface Encoder {
  public Properties getSlotOptions();
  public String getName();
}
