package org.postgresql.annotation;

import org.postgresql.configuration.PostgresLogicalDecoderListenerRegistrar;

public interface PostgresLogicalDecoderConfigurer {
  void configureListeners(PostgresLogicalDecoderListenerRegistrar registrar);

}
