package org.postgresql.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Container annotation that aggregates several {@link PostgresLogicalDecoderListener} annotations.
 * <p>
 * Can be used natively, declaring several nested {@link PostgresLogicalDecoderListener} annotations.
 * Can also be used in conjunction with Java 8's support for repeatable annotations,
 * where {@link PostgresLogicalDecoderListener} can simply be declared several times on the same method
 * (or class), implicitly generating this container annotation.
 *
 * @author Dave Cramer
 *
 * @see PostgresLogicalDecoderListener
 */
@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PostgresLogicalDecoderListeners {
  PostgresLogicalDecoderListener [] value();
}
