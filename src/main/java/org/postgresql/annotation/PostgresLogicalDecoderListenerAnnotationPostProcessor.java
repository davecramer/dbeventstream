package org.postgresql.annotation;

import org.postgresql.configuration.MethodPostgresLogicalDecodingListenerSlot;
import org.postgresql.configuration.PostgresLogicalDecoderListenerRegistrar;
import org.postgresql.listener.PostgresLogicalDecoderErrorHandler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.beans.factory.config.BeanExpressionContext;
import org.springframework.beans.factory.config.BeanExpressionResolver;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.expression.StandardBeanExpressionResolver;
import org.springframework.core.MethodIntrospector;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@EnablePostgreSQL
@Scope("singleton")
public class PostgresLogicalDecoderListenerAnnotationPostProcessor <K,V> implements BeanPostProcessor,
    Ordered,
    BeanFactoryAware,
    SmartInitializingSingleton {

  private BeanFactory beanFactory;

  PostgresLogicalDecoderListenerRegistrar registrar = new PostgresLogicalDecoderListenerRegistrar();

  private final Set<Class<?>> nonAnnotatedClasses =
      Collections.newSetFromMap(new ConcurrentHashMap<Class<?>, Boolean>(64));

  private final Log logger = LogFactory.getLog(getClass());

  private BeanExpressionResolver resolver = new StandardBeanExpressionResolver();

  private BeanExpressionContext expressionContext;

  public PostgresLogicalDecoderListenerAnnotationPostProcessor(){

  }

  /**
   * Callback that supplies the owning factory to a bean instance.
   * <p>Invoked after the population of normal bean properties
   * but before an initialization callback such as {@link InitializingBean#afterPropertiesSet()} or
   * a custom init-method.
   *
   * @param beanFactory owning BeanFactory (never {@code null}). The bean can immediately call
   *                    methods on the factory.
   * @throws BeansException in case of initialization errors
   * @see BeanInitializationException
   */
  @Override
  public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
    this.beanFactory = beanFactory;
  }

  /**
   * Invoked right at the end of the singleton pre-instantiation phase, with a guarantee that all
   * regular singleton beans have been created already. {@link ListableBeanFactory#getBeansOfType}
   * calls within this method won't trigger accidental side effects during bootstrap.
   * <p><b>NOTE:</b> This callback won't be triggered for singleton beans
   * lazily initialized on demand after {@link BeanFactory} bootstrap, and not for any other bean
   * scope either. Carefully use it for beans with the intended bootstrap semantics only.
   */
  @Override
  public void afterSingletonsInstantiated() {
    if (this.beanFactory instanceof ListableBeanFactory) {
      Map<String, PostgresLogicalDecoderConfigurer> instances =
          ((ListableBeanFactory) this.beanFactory).getBeansOfType(PostgresLogicalDecoderConfigurer.class);
      for (PostgresLogicalDecoderConfigurer configurer : instances.values()) {
        configurer.configureListeners(this.registrar);
      }
    }




  }

  /**
   * Get the order value of this object.
   * <p>Higher values are interpreted as lower priority. As a consequence,
   * the object with the lowest value has the highest priority (somewhat analogous to Servlet {@code
   * load-on-startup} values).
   * <p>Same order values will result in arbitrary sort positions for the
   * affected objects.
   *
   * @return the order value
   * @see #HIGHEST_PRECEDENCE
   * @see #LOWEST_PRECEDENCE
   */
  @Override
  public int getOrder() {
    return 0;
  }

  @Override
  public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
    return bean;
  }

  @Override
  public Object postProcessAfterInitialization(final Object bean, final String beanName) throws BeansException {
    if (!this.nonAnnotatedClasses.contains(bean.getClass())) {
      Class<?> targetClass = AopUtils.getTargetClass(bean);

      /*
      find methods annotated with PostgresLogicalDecoderListener
       */
      Map<Method, Set<PostgresLogicalDecoderListener>> annotatedMethods = MethodIntrospector.selectMethods(targetClass,
          new MethodIntrospector.MetadataLookup<Set<PostgresLogicalDecoderListener>>() {

            @Override
            public Set<PostgresLogicalDecoderListener> inspect(Method method) {
              Set<PostgresLogicalDecoderListener> listenerMethods = findListenerAnnotations(method);
              return (!listenerMethods.isEmpty() ? listenerMethods : null);
            }

          });
      if (annotatedMethods.isEmpty()) {
        this.nonAnnotatedClasses.add(bean.getClass());
        if (this.logger.isTraceEnabled()) {
          this.logger.trace("No @PostgresLogicalDecoderListener annotations found on bean type: " + bean.getClass());
        }
      }
      else {
        // Non-empty set of methods
        for (Map.Entry<Method, Set<PostgresLogicalDecoderListener>> entry : annotatedMethods.entrySet()) {
          Method method = entry.getKey();
          for (PostgresLogicalDecoderListener listener : entry.getValue()) {
            processLogicalDecoderListener(listener, method, bean, beanName);
          }
        }
        if (this.logger.isDebugEnabled()) {
          this.logger.debug(annotatedMethods.size() + " @PostgresLogicalDecoderListener methods processed on bean '"
              + beanName + "': " + annotatedMethods);
        }
      }
    }
    return bean;
  }

  /*
   * AnnotationUtils.getRepeatableAnnotations does not look at interfaces
   */
  private Set<PostgresLogicalDecoderListener> findListenerAnnotations(Method method) {
    Set<PostgresLogicalDecoderListener> listeners = new HashSet<PostgresLogicalDecoderListener>();
    PostgresLogicalDecoderListener ann = AnnotationUtils.findAnnotation(method, PostgresLogicalDecoderListener.class);
    if (ann != null) {
      listeners.add(ann);
    }
    PostgresLogicalDecoderListeners anns = AnnotationUtils.findAnnotation(method, PostgresLogicalDecoderListeners.class);
    if (anns != null) {
      listeners.addAll(Arrays.asList(anns.value()));
    }
    return listeners;
  }


  protected void processLogicalDecoderListener(PostgresLogicalDecoderListener postgresLogicalDecoderListener,
      Method method, Object bean, String beanName) {

    Method methodToUse = checkProxy(method, bean);
    MethodPostgresLogicalDecodingListenerSlot<K, V> slot = new MethodPostgresLogicalDecodingListenerSlot<K, V>();
    slot.setMethod(methodToUse);
    slot.setBeanFactory(this.beanFactory);
    String errorHandlerBeanName = resolveExpressionAsString(postgresLogicalDecoderListener.errorHandler(), "errorHandler");
    if (StringUtils.hasText(errorHandlerBeanName)) {
      slot.setErrorHandler(this.beanFactory.getBean(errorHandlerBeanName,
          PostgresLogicalDecoderErrorHandler.class));
    }
    processListener(slot, postgresLogicalDecoderListener, bean, methodToUse, beanName);
  }
  private Method checkProxy(Method methodArg, Object bean) {
    Method method = methodArg;
    if (AopUtils.isJdkDynamicProxy(bean)) {
      try {
        // Found a @PostgresLogicalDecoderListener method on the target class for this JDK proxy ->
        // is it also present on the proxy itself?
        method = bean.getClass().getMethod(method.getName(), method.getParameterTypes());
        Class<?>[] proxiedInterfaces = ((Advised) bean).getProxiedInterfaces();
        for (Class<?> iface : proxiedInterfaces) {
          try {
            method = iface.getMethod(method.getName(), method.getParameterTypes());
            break;
          }
          catch (NoSuchMethodException noMethod) {
          }
        }
      }
      catch (SecurityException ex) {
        ReflectionUtils.handleReflectionException(ex);
      }
      catch (NoSuchMethodException ex) {
        throw new IllegalStateException(String.format(
            "@PostgresLogicalDecoderListener method '%s' found on bean target class '%s', " +
                "but not found in any interface(s) for bean JDK proxy. Either " +
                "pull the method up to an interface or switch to subclass (CGLIB) " +
                "proxies by setting proxy-target-class/proxyTargetClass " +
                "attribute to 'true'", method.getName(), method.getDeclaringClass().getSimpleName()), ex);
      }
    }
    return method;
  }


  protected void processListener(MethodPostgresLogicalDecodingListenerSlot<?, ?> slot,
      PostgresLogicalDecoderListener postgresLogicalDecoderListener, Object bean,
      Object adminTarget, String beanName) {


    ((ConfigurableListableBeanFactory)beanFactory).registerSingleton("slotListener",
        registrar.addListener(slot, bean,  postgresLogicalDecoderListener));

  }

  private String resolveExpressionAsString(String value, String attribute) {
    Object resolved = resolveExpression(value);
    if (resolved instanceof String) {
      return (String) resolved;
    }
    else {
      throw new IllegalStateException("The [" + attribute + "] must resolve to a String. "
          + "Resolved to [" + resolved.getClass() + "] for [" + value + "]");
    }
  }

  private Object resolveExpression(String value) {
    String resolvedValue = resolve(value);

    return this.resolver.evaluate(resolvedValue, this.expressionContext);
  }

  /**
   * Resolve the specified value if possible.
   * @param value the value to resolve
   * @return the resolved value
   * @see ConfigurableBeanFactory#resolveEmbeddedValue
   */
  private String resolve(String value) {
    if (this.beanFactory != null && this.beanFactory instanceof ConfigurableBeanFactory) {
      return ((ConfigurableBeanFactory) this.beanFactory).resolveEmbeddedValue(value);
    }
    return value;
  }

}
