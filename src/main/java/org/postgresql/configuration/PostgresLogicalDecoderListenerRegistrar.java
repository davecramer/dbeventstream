package org.postgresql.configuration;

import org.postgresql.annotation.PostgresLogicalDecoderListener;
import org.postgresql.listener.PostgresLogicalDecoderListenerContainer;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class PostgresLogicalDecoderListenerRegistrar {

  Map <String,PostgresLogicalDecoderListenerContainer> containerMap = new HashMap<>();

  public PostgresLogicalDecoderListenerContainer addListener(MethodPostgresLogicalDecodingListenerSlot  slot, Object bean,
      PostgresLogicalDecoderListener listener) {
    PostgresLogicalDecoderListenerContainer container = containerMap.get(listener.slotName());
    if ( container == null ){
      try {
        container = new PostgresLogicalDecoderListenerContainer(slot, bean, listener);
        containerMap.put(listener.slotName(),container);
      } catch (SQLException e) {
        e.printStackTrace();
      }
    } else {
      container.addListener(listener);
    }
    return container;
  }
}
