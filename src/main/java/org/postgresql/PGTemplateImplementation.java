package org.postgresql;

import org.postgresql.core.BaseConnection;
import org.postgresql.core.ServerVersion;
import org.postgresql.jdbc.PgConnection;
import org.postgresql.replication.LogSequenceNumber;
import org.postgresql.replication.PGReplicationStream;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.Lifecycle;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.nio.ByteBuffer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.sql.DataSource;

public class PGTemplateImplementation extends JdbcTemplate implements PGTemplate, Lifecycle {

  PgConnection replicationConnection=null;

  PGTemplateImplementation(DataSource dataSource){
    super(dataSource);
  }

  /**
   * stop a slot from replicating.
   */
  @Override
  public boolean stopSlot(String slotName) throws SQLException {
    return query("select pg_terminate_backend(active_pid) from pg_replication_slots "
            + "where active = true and slot_name = ?",
        new PreparedStatementSetter() {
          @Override
          public void setValues(PreparedStatement ps) throws SQLException {
            ps.setString(1, slotName);
          }
        },
        new ResultSetExtractor<Boolean>() {
          @Override
          public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {
            if (rs.next()){
              return rs.getBoolean(1);
            }
            else {
              return false;
            }
          }
        });

  }

  @Override
  public boolean slotExists(String slotName) throws SQLException {
    return query("select * from pg_replication_slots where slot_name=?",
        new PreparedStatementSetter() {
          @Override
          public void setValues(PreparedStatement ps) throws SQLException {
            ps.setString(1, slotName);
          }
        },
        new ResultSetExtractor<Boolean>() {
          @Override
          public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {
            /*
            we are just looking to see if we have a slot named slotName, if there is a
            next then the slot exists
             */
            return rs.next();
          }
        }
    );
  }

  @Override
  public boolean dropReplicationSlot(String slotName) throws SQLException,InterruptedException, TimeoutException {

    if (slotExists(slotName)) {

      if (stopSlot(slotName)) {
        waitStopReplicationSlot(slotName);
      }

      return query("select pg_drop_replication_slot(slot_name) "
              + "from pg_replication_slots where slot_name = ?",

          new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
              ps.setString(1, slotName);
            }
          },
          new ResultSetExtractor<Boolean>() {
            @Override
            public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {
              return rs.next();
            }
      });
    }else {
      return false;
    }

  }

  /**
   *
   * @param slotName provide a name for the slot
   * @param outputPlugin use this output plugin to encode the transactions*
   * @return the current @LogSequenceNumber
   * @throws SQLException
   */
  @Override
  public LogSequenceNumber createLogicalReplicationSlot(String slotName, String outputPlugin) throws SQLException
  {
    //drop previous slot
    //dropReplicationSlot(connection, slotName);

    return query("SELECT * FROM pg_create_logical_replication_slot(?, ?)",
        new PreparedStatementSetter() {
          @Override
          public void setValues(PreparedStatement ps) throws SQLException {
            ps.setString(1, slotName);
            ps.setString(2, outputPlugin);
          }
        },
        new ResultSetExtractor<LogSequenceNumber>() {
            public LogSequenceNumber extractData( ResultSet rs ) throws SQLException,
                DataAccessException {
              if ( rs.next() ){
                return LogSequenceNumber.valueOf(rs.getString(2));
              }
              else {
                return LogSequenceNumber.INVALID_LSN;
              }
            }
        });
  }

  /**
   *
   * @param slotName
   * @return true if @slotName is active
   * @throws SQLException
   */
  @Override
  public boolean isReplicationSlotActive(String slotName) throws SQLException
  {
    return query("select active from pg_replication_slots where slot_name = ?",
        new PreparedStatementSetter() {
          @Override
          public void setValues(PreparedStatement ps) throws SQLException {
            ps.setString(1, slotName);
          }
        },
        new ResultSetExtractor<Boolean>() {
          @Override
          public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {
            if (rs.next() ){
              return rs.getBoolean(1);
            }else{
              return false;
            }
          }
        });
  }

  /**
   *
   * @param slotName this is the name of the slot that was previously defined in createLogicalReplicationSlot
   * @param lsn starting Log Sequence Number @see LogSequenceNumber
   * @param slotOptions Each Logical decoder will take options, see the docs for the decoder you used in createLogicalReplicationSlot
   * @return
   * @throws SQLException
   */
  @Override
  public PGReplicationStream getReplicationStream(String slotName, LogSequenceNumber lsn,
      Properties slotOptions) throws SQLException
  {
    replicationConnection = (PgConnection)DataSourceUtils.getConnection(obtainDataSource());
    if (lsn == null){
      lsn = getCurrentLSN();
    }

    return  replicationConnection
        .getReplicationAPI()
        .replicationStream()
        .logical()
        .withSlotName(slotName)
        .withStartPosition(lsn)
        .withSlotOptions(slotOptions)
        .withStatusInterval(10, TimeUnit.SECONDS)
        .start();
  }

  /**
   *
   * @param stream
   * @param logicalDecoder
   * @param <T>
   * @return T if there is anything to read, otherwise null
   * @throws SQLException
   */
  @Override
  public <T> T readStream(PGReplicationStream stream, LogicalDecoder<T> logicalDecoder) throws SQLException
  {
    ByteBuffer buffer;
    buffer = stream.readPending();
    if (buffer != null ) {
      int offset = buffer.arrayOffset();
      byte [] buf = buffer.array();
      int length = buf.length - offset;

      return logicalDecoder.decode(buf, offset, length );
    }
    return null;
  }

  @Override
  public void dropPublication(String publication) throws SQLException {
    execute( "DROP PUBLICATION " + publication );
  }

  @Override
  public void createPublication(String publication) throws SQLException {
    execute("CREATE PUBLICATION " + publication + " FOR ALL TABLES");
  }

  private boolean haveMinimumServerVersion(ServerVersion serverVersion) {
    return ((BaseConnection)DataSourceUtils.getConnection(obtainDataSource())).haveMinimumServerVersion(serverVersion);
  }

  private  void waitStopReplicationSlot(String slotName)
      throws InterruptedException, TimeoutException, SQLException
  {
    long startWaitTime = System.currentTimeMillis();
    boolean stillActive;
    long timeInWait = 0;

    do {
      stillActive = isReplicationSlotActive(slotName);
      if (stillActive) {
        TimeUnit.MILLISECONDS.sleep(100L);
        timeInWait = System.currentTimeMillis() - startWaitTime;
      }
    } while (stillActive && timeInWait <= 30000);

    if (stillActive) {
      throw new TimeoutException("Timeout waiting for the replication slot " + slotName + " to stop wait time: " + timeInWait);
    }
  }

  private LogSequenceNumber getCurrentLSN() throws SQLException
  {

    return query("select "
        + (haveMinimumServerVersion(ServerVersion.v10)
        ? "pg_current_wal_lsn()" : "pg_current_xlog_location()"), new ResultSetExtractor<LogSequenceNumber>() {

      @Override
      public LogSequenceNumber extractData(ResultSet rs) throws SQLException, DataAccessException {
        if (rs.next()) {
          String lsn = rs.getString(1);
          return LogSequenceNumber.valueOf(lsn);
        } else {
          return LogSequenceNumber.INVALID_LSN;
        }
      }
    });
  }

  /**
   * Start this component.
   * <p>Should not throw an exception if the component is already running.
   * <p>In the case of a container, this will propagate the start signal to all
   * components that apply.
   *
   * @see SmartLifecycle#isAutoStartup()
   */
  @Override
  public void start() {

  }

  /**
   * Stop this component, typically in a synchronous fashion, such that the component is fully
   * stopped upon return of this method. Consider implementing {@link SmartLifecycle} and its {@code
   * stop(Runnable)} variant when asynchronous stop behavior is necessary.
   * <p>Note that this stop notification is not guaranteed to come before destruction: On
   * regular shutdown, {@code Lifecycle} beans will first receive a stop notification before the
   * general destruction callbacks are being propagated; however, on hot refresh during a context's
   * lifetime or on aborted refresh attempts, only destroy methods will be called.
   * <p>Should not throw an exception if the component isn't started yet.
   * <p>In the case of a container, this will propagate the stop signal to all components
   * that apply.
   *
   * @see SmartLifecycle#stop(Runnable)
   * @see DisposableBean#destroy()
   */
  @Override
  public void stop() {

  }

  /**
   * Check whether this component is currently running.
   * <p>In the case of a container, this will return {@code true} only if <i>all</i>
   * components that apply are currently running.
   *
   * @return whether the component is currently running
   */
  @Override
  public boolean isRunning() {
    return false;
  }
}
